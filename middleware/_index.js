const errorHandlerMiddleware = require('./error-handler')
const notFound = require('./not-found')


exports.errorHander = errorHandlerMiddleware
exports.notFound = notFound